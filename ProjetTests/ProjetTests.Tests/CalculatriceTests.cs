
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProjetTests.Tests
{
    [TestClass]
    public class CalculatriceTests
    {
        [TestMethod]
        public void Calling_Addition_With_3_And_5_Should_Return_8()
        {
            var calculatrice = new Calculatrice();
            var result = calculatrice.Addition(3, 5);
            Assert.AreEqual(8, result);
        }
    }
}